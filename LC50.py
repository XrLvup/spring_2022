# Problem description:   Group Anagrams
"""
Given an array of strings strs, group the anagrams together. You can return the answer in any order.

An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase, typically using all the original letters exactly once.



Example 1:
Input: strs = ["eat","tea","tan","ate","nat","bat"]
Output: [["bat"],["nat","tan"],["ate","eat","tea"]]

Example 2:
Input: strs = [""]
Output: [[""]]

Example 3:
Input: strs = ["a"]
Output: [["a"]]


Constraints:

1 <= strs.length <= 104
0 <= strs[i].length <= 100
strs[i] consists of lowercase English letters.
"""
from typing import List


# Time complexity
# m - length of max string, n - no of strings in list
# space = O(nm)
# time - so nmlogm

def groupAnagrams(strs: List[str]) -> List[List[str]]:
    sorted_key_dict = {}
    for st in strs:
        sorted_st = sorted(st)
        sorted_st = "".join(sorted_st)
        # print(sorted_st)
        if sorted_st in sorted_key_dict:
            sorted_key_dict[sorted_st].append(st)
        else:
            sorted_key_dict[sorted_st] = [st]
    return sorted_key_dict.values()

