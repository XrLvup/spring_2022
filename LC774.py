# Problem description:  Minimize Max Distance to Gas Station
"""
You are given an integer array stations that represents the positions of the gas stations on the x-axis. You are also given an integer k.

You should add k new gas stations. You can add the stations anywhere on the x-axis, and not necessarily on an integer position.

Let penalty() be the maximum distance between adjacent gas stations after adding the k new stations.

Return the smallest possible value of penalty(). Answers within 10-6 of the actual answer will be accepted.



Example 1:

Input: stations = [1,2,3,4,5,6,7,8,9,10], k = 9
Output: 0.50000
Example 2:

Input: stations = [23,24,36,39,46,56,57,65,84,98], k = 1
Output: 14.00000


Constraints:

10 <= stations.length <= 2000
0 <= stations[i] <= 108
stations is sorted in a strictly increasing order.
1 <= k <= 106
"""
from typing import List
import math


# running time: O(nlog(# of range))
# space: O(1)

def minmaxGasDist(stations: List[int], k: int) -> float:
    start = 0
    end = 0
    for i in range(0, len(stations) - 1):
        end = max(end, stations[i + 1] - stations[i])
    tolerance = math.pow(10, -6)
    while start <= end - tolerance:
        mid = start + (end - start) / 2
        #print(f"start={start}, end={end}, mid={mid}")
        if find_k(stations, mid) <= k:
            end = mid
        else:  # <= k
            start = mid
    return start


def find_k(stations: List[int], penalty: float) -> int:
    k = 0
    for i in range(0, len(stations) - 1):
        if stations[i + 1] - stations[i] > penalty:
            k += math.ceil((stations[i + 1] - stations[i]) // penalty)
    #print(f"k={k}")
    return k