# Problem description:  Permutation in String
"""
Given two strings s1 and s2, return true if s2 contains a permutation of s1, or false otherwise.

In other words, return true if one of s1's permutations is the substring of s2.



Example 1:
Input: s1 = "ab", s2 = "eidbaooo"
Output: true
Explanation: s2 contains one permutation of s1 ("ba").

Example 2:
Input: s1 = "ab", s2 = "eidboaoo"
Output: false


Constraints:

1 <= s1.length, s2.length <= 104
s1 and s2 consist of lowercase English letters.
"""

# running time: O(n)
# space: O(k)
from typing import List


def checkInclusion(s1: str, s2: str) -> bool:
    s1_dict = {}
    for i in range(0, len(s1)):
        if s1[i] not in s1_dict:
            s1_dict[s1[i]] = 1
        else:
            s1_dict[s1[i]] += 1
    s2_dict = {}
    k = len(s1)
    if k > len(s2):
        return 0
    # first sliding window
    for i in range(0, k):
        if s2[i] not in s2_dict:
            s2_dict[s2[i]] = 1
        else:
            s2_dict[s2[i]] += 1
    if s2_dict == s1_dict:
        return True
    # rest of sliding window
    for i in range(k, len(s2)):
        if s2[i] not in s2_dict:
            s2_dict[s2[i]] = 1
        else:
            s2_dict[s2[i]] += 1
        # remove left most entry
        s2_dict[s2[i - k]] -= 1
        if s2_dict[s2[i - k]] == 0:
            del s2_dict[s2[i - k]]
        if s2_dict == s1_dict:
            return True
    return False
