# Problem description: Missing Number In Arithmetic Progression
"""
In some array arr, the values were in arithmetic progression: the values arr[i + 1] - arr[i] are all equal for every 0 <= i < arr.length - 1.

A value from arr was removed that was not the first or last value in the array.

Given arr, return the removed value.



Example 1:

Input: arr = [5,7,11,13]
Output: 9
Explanation: The previous array was [5,7,9,11,13].

Example 2:
Input: arr = [15,13,12]
Output: 14
Explanation: The previous array was [15,14,13,12].


Constraints:

3 <= arr.length <= 1000
0 <= arr[i] <= 105
The given array is guaranteed to be a valid array.
"""
from typing import List


# running time: O(log(n))
# space: O(1)

def missingNumber(arr: List[int]) -> int:
    seq_diff = (arr[-1] - arr[0]) // len(arr)
    start = 0
    end = len(arr) - 1
    while start <= end:
        mid = start + (end - start) // 2
        # print(mid)
        if arr[0] + (seq_diff * mid) == arr[mid]:
            # then no missing value seen so far
            start = mid + 1
        else:
            end = mid - 1
            # start is pointing to the index whose value should be
    return arr[0] + (seq_diff * start)
