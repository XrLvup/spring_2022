# Problem description:  Koko Eating Bananas
"""
Koko loves to eat bananas. There are n piles of bananas, the ith pile has piles[i] bananas. The guards have gone and will come back in h hours.

Koko can decide her bananas-per-hour eating speed of k. Each hour, she chooses some pile of bananas and eats k bananas from that pile. If the pile has less than k bananas, she eats all of them instead and will not eat any more bananas during this hour.

Koko likes to eat slowly but still wants to finish eating all the bananas before the guards return.

Return the minimum integer k such that she can eat all the bananas within h hours.



Example 1:

Input: piles = [3,6,7,11], h = 8
Output: 4

Example 2:
Input: piles = [30,11,23,4,20], h = 5
Output: 30

Example 3:
Input: piles = [30,11,23,4,20], h = 6
Output: 23


Constraints:

1 <= piles.length <= 104
piles.length <= h <= 109
1 <= piles[i] <= 109
"""
from typing import List
import math

# running time: O(nlog(m)) where n = length of the piles array and m = max_value in the piles array
# space: O(1)

def minEatingSpeed(piles: List[int], h: int) -> int:
    # finding the range of k
    start = 1  # min value
    end = max(piles)  # max value as per question

    while start <= end:
        mid = start + (end - start) // 2
        # print(f"start={start}, end={end}, mid ={mid}")
        # 2 zones are to find the min k ( > h & <= h )
        if geth(piles, mid) > h:
            start = mid + 1
        else:  # <= h
            end = mid - 1
    return start

"""
each iteration of the binary search finding value of h
takes O(n) time
"""

def geth(piles: List[int], k: int) -> int:
    h = 0
    for i in range(0, len(piles)):
        h += math.ceil(piles[i] / k)
    # print(f"h={h}")
    return h


