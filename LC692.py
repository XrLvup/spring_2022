# Problem description:  Top K Frequent Words
"""
Given an array of strings words and an integer k, return the k most frequent strings.

Return the answer sorted by the frequency from highest to lowest. Sort the words with the same frequency by their lexicographical order.



Example 1:

Input: words = ["i","love","leetcode","i","love","coding"], k = 2
Output: ["i","love"]
Explanation: "i" and "love" are the two most frequent words.
Note that "i" comes before "love" due to a lower alphabetical order.

Example 2:
Input: words = ["the","day","is","sunny","the","the","the","sunny","is","is"], k = 4
Output: ["the","is","sunny","day"]
Explanation: "the", "is", "sunny" and "day" are the four most frequent words, with the number of occurrence being 4, 3, 2 and 1 respectively.


Constraints:

1 <= words.length <= 500
1 <= words[i] <= 10
words[i] consists of lowercase English letters.
k is in the range [1, The number of unique words[i]]


Follow-up: Could you solve it in O(n log(k)) time and O(n) extra space?
"""
from typing import List
import heapq

# running time: O(nlogk) + (klogk) = O(nlogk)
# space: O(k) - k items in heap

def topKFrequent(words: List[str], k: int) -> List[str]:
    # creating custom node class to insert into heap
    class Node(object):
        def __init__(self, word: str, freq: int):
            self.word = word
            self.freq = freq

        def __repr__(self):
            return f'Node value: {self.val}'

        def __lt__(self, other):
            if self.freq < other.freq:
                return True
            if self.freq > other.freq:
                return False
            if self.word > other.word:
                return True

    word_dict = {}
    for word in words:
        if word not in word_dict:
            word_dict[word] = 1
        else:
            word_dict[word] += 1

    node_objs = []
    for key in word_dict:
        if len(node_objs) < k:
            heapq.heappush(node_objs, Node(key, word_dict[key]))
        else:
            node = node_objs[0]
            cur_node = Node(key, word_dict[key])
            if node < cur_node:
                heapq.heappop(node_objs)
                heapq.heappush(node_objs, cur_node)
    # nlogk = looping at n objects and keeping track of top k in heap
    # sorting k = k logk
    # total time = nlogk + klogk = nlogk
    # space = k
    node_objs.sort(reverse=True)
    return list(map(lambda x: x.word, node_objs))
