# Problem description:  Meeting Rooms
"""
Given an array of meeting time intervals where intervals[i] = [starti, endi], determine if a person could attend all meetings.



Example 1:

Input: intervals = [[0,30],[5,10],[15,20]]
Output: false

Example 2:
Input: intervals = [[7,10],[2,4]]
Output: true


Constraints:

0 <= intervals.length <= 104
intervals[i].length == 2
0 <= starti < endi <= 106
"""
from typing import List


# Running time: O(n) - linear scan + O(nlogn) - sort
#               O(nlogn)
# Space: O(1)
def canAttendMeetings(self, intervals: List[List[int]]) -> bool:
    def check_overlap(interval1: List[int], interval2: List[int]) -> bool:
        start1 = interval1[0]
        end1 = interval1[1]
        start2 = interval2[0]
        end2 = interval2[1]
        if end1 > start2:
            # there is overlap
            return True
        return False

    # sort by start time
    intervals.sort(key=lambda a: a[0])
    # iterate through list checking for overlap
    for i in range(0, len(intervals) - 1):
        first = intervals[i]
        second = intervals[i + 1]
        if check_overlap(first, second):
            return False
    return True

