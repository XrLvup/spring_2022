# Problem description:  Range Sum Query 2D - Immutable
"""
Given a 2D matrix matrix, handle multiple queries of the following type:

Calculate the sum of the elements of matrix inside the rectangle defined by its upper left corner (row1, col1) and lower right corner (row2, col2).
Implement the NumMatrix class:

NumMatrix(int[][] matrix) Initializes the object with the integer matrix matrix.
int sumRegion(int row1, int col1, int row2, int col2) Returns the sum of the elements of matrix inside the rectangle defined by its upper left corner (row1, col1) and lower right corner (row2, col2).


Example 1:

Input
["NumMatrix", "sumRegion", "sumRegion", "sumRegion"]
[[[[3, 0, 1, 4, 2], [5, 6, 3, 2, 1], [1, 2, 0, 1, 5], [4, 1, 0, 1, 7], [1, 0, 3, 0, 5]]], [2, 1, 4, 3], [1, 1, 2, 2], [1, 2, 2, 4]]
Output
[null, 8, 11, 12]
Explanation
NumMatrix numMatrix = new NumMatrix([[3, 0, 1, 4, 2], [5, 6, 3, 2, 1], [1, 2, 0, 1, 5], [4, 1, 0, 1, 7], [1, 0, 3, 0, 5]]);
numMatrix.sumRegion(2, 1, 4, 3); // return 8 (i.e sum of the red rectangle)
numMatrix.sumRegion(1, 1, 2, 2); // return 11 (i.e sum of the green rectangle)
numMatrix.sumRegion(1, 2, 2, 4); // return 12 (i.e sum of the blue rectangle)


Constraints:

m == matrix.length
n == matrix[i].length
1 <= m, n <= 200
-105 <= matrix[i][j] <= 105
0 <= row1 <= row2 < m
0 <= col1 <= col2 < n
At most 104 calls will be made to sumRegion.
"""

# Space = O(nm) -
# Time = O(nm) - construction, O(1) - query
from typing import List


class NumMatrix:

    def __init__(self, matrix: List[List[int]]):
        r = len(matrix)
        c = len(matrix[0])
        # table = [[0 for c in range(n)] for r in range(n)]
        self.prefix_sums = [[0] * c for _ in range(r)]
        self.prefix_sums[0][0] = matrix[0][0]
        # update  all columns in 1st row
        for i in range(1, c):
            self.prefix_sums[0][i] += self.prefix_sums[0][i - 1] + matrix[0][i]
        # update all rows in 1st col
        for i in range(1, r):
            self.prefix_sums[i][0] += self.prefix_sums[i - 1][0] + matrix[i][0]
        # rest of the cells
        for i in range(1, r):
            for j in range(1, c):
                # print(self.prefix_sums)
                # looking at prev row entry , prev col entry and subtracting overlap
                self.prefix_sums[i][j] = matrix[i][j] + self.prefix_sums[i][j - 1] + self.prefix_sums[i - 1][j] - \
                                         self.prefix_sums[i - 1][j - 1]
        # print(self.prefix_sums)

    def sumRegion(self, row1: int, col1: int, row2: int, col2: int) -> int:
        # edge cases
        if row1 == 0 and col1 == 0:
            return self.prefix_sums[row2][col2]
        if row1 == 0:
            return self.prefix_sums[row2][col2] - self.prefix_sums[row2][col1 - 1]
        if col1 == 0:
            return self.prefix_sums[row2][col2] - self.prefix_sums[row1 - 1][col2]
        return self.prefix_sums[row2][col2] - self.prefix_sums[row2][col1 - 1] - self.prefix_sums[row1 - 1][col2] + \
               self.prefix_sums[row1 - 1][col1 - 1]

