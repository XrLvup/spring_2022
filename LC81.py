# Problem description:  Search in Rotated Sorted Array II
"""
There is an integer array nums sorted in non-decreasing order (not necessarily with distinct values).

Before being passed to your function, nums is rotated at an unknown pivot index k (0 <= k < nums.length) such that the resulting array is [nums[k], nums[k+1], ..., nums[n-1], nums[0], nums[1], ..., nums[k-1]] (0-indexed). For example, [0,1,2,4,4,4,5,6,6,7] might be rotated at pivot index 5 and become [4,5,6,6,7,0,1,2,4,4].

Given the array nums after the rotation and an integer target, return true if target is in nums, or false if it is not in nums.

You must decrease the overall operation steps as much as possible.



Example 1:

Input: nums = [2,5,6,0,0,1,2], target = 0
Output: true
Example 2:

Input: nums = [2,5,6,0,0,1,2], target = 3
Output: false
"""
from typing import List


# running time: O(n)
# space: O(1)

def search(nums: List[int], target: int) -> bool:
    start = 0
    # remove duplicates if exists from the beginning , then this reduces to LC33
    while start < len(nums) and nums[start] == nums[-1]:
        start += 1
    if start >= len(nums):
        return nums[0] == target
    target_zone = None
    if target <= nums[-1]:
        target_zone = 2
    else:
        target_zone = 1
    end = len(nums) - 1
    while start <= end:
        mid = start + (end - start) // 2
        if nums[mid] == target:
            return True
        elif nums[mid] <= nums[-1] and target_zone == 1:
            end = mid - 1
        elif nums[mid] > nums[-1] and target_zone == 2:
            start = mid + 1
        else:
            if nums[mid] < target:
                start = mid + 1
            else:
                end = mid - 1
    return False

