# Problem description:  Valid Perfect Square
"""
Given a positive integer num, write a function which returns True if num is a perfect square else False.

Follow up: Do not use any built-in library function such as sqrt.



Example 1:

Input: num = 16
Output: true

Example 2:
Input: num = 14
Output: false


Constraints:

1 <= num <= 2^31 - 1

"""
from typing import List


# running time: O(log(N))
# space: O(1)

def isPerfectSquare(num: int) -> bool:
    if num < 2:
        return True
    start = 2
    end = num // 2
    while start <= end:
        mid = start + (end - start) // 2
        if mid * mid == num:
            return True
        elif mid * mid < num:
            start = mid + 1
        else:
            end = mid - 1
    return False

