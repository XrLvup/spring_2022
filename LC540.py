# Problem description:  Single Element in a Sorted Array
"""
You are given a sorted array consisting of only integers where every element appears exactly twice, except for one element which appears exactly once.

Return the single element that appears only once.

Your solution must run in O(log n) time and O(1) space.



Example 1:

Input: nums = [1,1,2,3,3,4,4,8,8]
Output: 2

Example 2:
Input: nums = [3,3,7,7,10,11,11]
Output: 10


Constraints:

1 <= nums.length <= 105
0 <= nums[i] <= 105
"""

# running time: O(log(n))
# space: O(1)
from typing import List


def singleNonDuplicate(nums: List[int]) -> int:
    start = 0
    end = len(nums) - 1
    while start <= end:
        mid = start + (end - start) // 2
        # print(f"start={start}, end= {end}, mid={mid}")
        if mid == 0 or mid == len(nums) - 1 or (nums[mid] != nums[mid - 1] and nums[mid] != nums[mid + 1]):
            # check if first element/last element
            return nums[mid]

        elif (mid % 2 == 0 and nums[mid] == nums[mid + 1]) or (
                mid % 2 != 0 and nums[mid - 1] == nums[mid]):
            # even-odd then no gap seen so far, so move right
            start = mid + 1

        else:
            # odd-even then gap seen so move left
            end = mid - 1
    return -1  # should not come here
