# Problem description:  Rotting Oranges
"""
You are given an m x n grid where each cell can have one of three values:

0 representing an empty cell,
1 representing a fresh orange, or
2 representing a rotten orange.
Every minute, any fresh orange that is 4-directionally adjacent to a rotten orange becomes rotten.

Return the minimum number of minutes that must elapse until no cell has a fresh orange. If this is impossible, return -1.



Example 1:
Input: grid = [[2,1,1],[1,1,0],[0,1,1]]
Output: 4

Example 2:
Input: grid = [[2,1,1],[0,1,1],[1,0,1]]
Output: -1
Explanation: The orange in the bottom left corner (row 2, column 0) is never rotten, because rotting only happens 4-directionally.

Example 3:
Input: grid = [[0,2]]
Output: 0
Explanation: Since there are already no fresh oranges at minute 0, the answer is just 0.


Constraints:

m == grid.length
n == grid[i].length
1 <= m, n <= 10
grid[i][j] is 0, 1, or 2
"""
from typing import List



# running time: O(mn)
# space: O(mn)
from collections import deque

def orangesRotting(grid: List[List[int]]) -> int:
    def get_neighbors(row: int, col: int) -> List[int]:
        return [(row + 1, col), (row, col + 1), (row - 1, col), (row, col - 1)]

    def isValidRow(row: int) -> bool:
        if row >= 0 and row < len(grid):
            return True

    def isValidCol(col: int) -> bool:
        if col >= 0 and col < len(grid[0]):
            return True

    def bfs(start_list: List) -> int:
        queue = deque([])
        dist = 0
        for start in start_list:
            queue.append((start[0], start[1], dist))

        while queue:
            row, col, dist = queue.popleft()
            neighbors_list = get_neighbors(row, col)
            for n_row, n_col in neighbors_list:
                if isValidRow(n_row) and isValidCol(n_col) and grid[n_row][n_col] == 1:
                    grid[n_row][n_col] = 2
                    queue.append((n_row, n_col, dist + 1))
        return dist

    rotten_oranges_list = []
    for i in range(0, len(grid)):
        for j in range(0, len(grid[0])):
            if grid[i][j] == 2:
                rotten_oranges_list.append((i, j))
    dist = bfs(rotten_oranges_list)
    # check for fresh orange
    for i in range(0, len(grid)):
        for j in range(0, len(grid[0])):
            if grid[i][j] == 1:
                return -1
    return dist



