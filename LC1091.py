# Problem description:   Shortest Path in Binary Matrix
"""
Given an n x n binary matrix grid, return the length of the shortest clear path in the matrix. If there is no clear path, return -1.

A clear path in a binary matrix is a path from the top-left cell (i.e., (0, 0)) to the bottom-right cell (i.e., (n - 1, n - 1)) such that:

All the visited cells of the path are 0.
All the adjacent cells of the path are 8-directionally connected (i.e., they are different and they share an edge or a corner).
The length of a clear path is the number of visited cells of this path.



Example 1:
Input: grid = [[0,1],[1,0]]
Output: 2

Example 2:
Input: grid = [[0,0,0],[1,1,0],[1,1,0]]
Output: 4

Example 3:
Input: grid = [[1,0,0],[1,1,0],[1,1,0]]
Output: -1


Constraints:
n == grid.length
n == grid[i].length
1 <= n <= 100
grid[i][j] is 0 or 1
"""
from typing import List

from collections import deque

# running time: O(mn)
# space: O(mn)

def shortestPathBinaryMatrix(grid: List[List[int]]) -> int:
    def get_neighbors(row: int, col: int) -> List[int]:
        return [(row - 1, col), (row + 1, col), (row, col - 1), (row, col + 1), (row + 1, col + 1), (row - 1, col - 1),
                (row + 1, col - 1), (row - 1, col + 1)]

    def isvalidRow(row: int) -> bool:
        if row >= 0 and row <= len(grid) - 1:
            return True
        return False

    def isvalidCol(col: int) -> bool:
        if col >= 0 and col <= len(grid[0]) - 1:
            return True
        return False

    if grid[0][0] == 1:
        return -1
    n = len(grid)
    m = len(grid[0])
    grid[0][0] = 1
    # visited_set = set([(0, 0)])
    queue = deque([(0, 0, 1)])
    while queue:
        (row, col, dist) = queue.popleft()
        if row == n - 1 and col == m - 1:
            return dist
        # print(f"Path = {row, col}")
        neighbors_list = get_neighbors(row, col)
        for n_row, n_col in neighbors_list:
            if isvalidRow(n_row) and isvalidCol(n_col) and grid[n_row][n_col] == 0:
                # mark as visited
                grid[n_row][n_col] = 1
                queue.append((n_row, n_col, dist + 1))
    # print(dist_map)
    return -1

