# Problem description:  Find Smallest Letter Greater Than Target
"""
Given a characters array letters that is sorted in non-decreasing order and a character target, return the smallest character in the array that is larger than target.

Note that the letters wrap around.

For example, if target == 'z' and letters == ['a', 'b'], the answer is 'a'.


Example 1:
Input: letters = ["c","f","j"], target = "a"
Output: "c"

Example 2:
Input: letters = ["c","f","j"], target = "c"
Output: "f"

Example 3:
Input: letters = ["c","f","j"], target = "d"
Output: "f"


Constraints:

2 <= letters.length <= 104
letters[i] is a lowercase English letter.
letters is sorted in non-decreasing order.
letters contains at least two different characters.
target is a lowercase English letter.
"""
from typing import List


# running time: O(log(n))
# space: O(1)

def nextGreatestLetter(letters: List[str], target: str) -> str:
    start = 0
    end = len(letters) - 1
    while start <= end:
        mid = start + (end - start) // 2
        if letters[mid] <= target:
            start = mid + 1
        else:
            end = mid - 1
    if start >= len(letters):
        return letters[0]
    return letters[start]