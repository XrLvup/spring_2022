# Problem description:  Backspace String Compare
"""
Given two strings s and t, return true if they are equal when both are typed into empty text editors. '#' means a backspace character.

Note that after backspacing an empty text, the text will continue empty.



Example 1:

Input: s = "ab#c", t = "ad#c"
Output: true
Explanation: Both s and t become "ac".

Example 2:
Input: s = "ab##", t = "c#d#"
Output: true
Explanation: Both s and t become "".

Example 3:
Input: s = "a#c", t = "b"
Output: false
Explanation: s becomes "c" while t becomes "b".


Constraints:

1 <= s.length, t.length <= 200
s and t only contain lowercase letters and '#' characters.


Follow up: Can you solve it in O(n) time and O(1) space?
"""


# running time: O(n + m)
# space: O(1)

def backspaceCompare(s: str, t: str) -> bool:
    i = len(s) - 1
    j = len(t) - 1
    s_skip_counter = 0
    t_skip_counter = 0
    while i >= 0 or j >= 0:
        while i >= 0 and (s[i] == '#' or s_skip_counter > 0):
            if s[i] == '#':
                s_skip_counter += 1
            else:
                s_skip_counter -= 1
            i -= 1
        while j >= 0 and (t[j] == '#' or t_skip_counter > 0):
            if t[j] == '#':
                t_skip_counter += 1
            else:
                t_skip_counter -= 1
            j -= 1
        if i >= 0 and j >= 0 and s[i] != t[j]:
            return False
        elif i >= 0 and s[i] != '#' and j < 0:
            return False
        elif j >= 0 and t[j] != '#' and i < 0:
            return False
        i -= 1
        j -= 1
    # print(i)
    # print(j)

    return True