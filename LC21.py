# Problem description:   Merge Two Sorted Lists
"""
You are given the heads of two sorted linked lists list1 and list2.

Merge the two lists in a one sorted list. The list should be made by splicing together the nodes of the first two lists.

Return the head of the merged linked list.



Example 1:

Input: list1 = [1,2,4], list2 = [1,3,4]
Output: [1,1,2,3,4,4]

Example 2:
Input: list1 = [], list2 = []
Output: []

Example 3:
Input: list1 = [], list2 = [0]
Output: [0]


Constraints:

Constraints:

The number of nodes in both lists is in the range [0, 50].
-100 <= Node.val <= 100
Both list1 and list2 are sorted in non-decreasing order.
"""
from typing import List, Optional


# running time: O(m+n)
# space: O(1)

# Definition for singly-linked list.
class ListNode:
     def __init__(self, val=0, next=None):
         self.val = val
         self.next = next

def mergeTwoLists(list1: Optional[ListNode], list2: Optional[ListNode]) -> Optional[ListNode]:
    sentinel = ListNode(-1)
    prev = sentinel
    first = list1
    second = list2
    while first is not None and second is not None:
        if first.val <= second.val:
            prev.next = first
            first = first.next
        else:  # second  :
            prev.next = second
            second = second.next
        prev = prev.next
    if first is None:
        prev.next = second
    else:
        prev.next = first
    return sentinel.next

