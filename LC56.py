# Problem description:   Merge Intervals
"""
Given an array of intervals where intervals[i] = [starti, endi], merge all overlapping intervals, and return an array of the non-overlapping intervals that cover all the intervals in the input.



Example 1:

Input: intervals = [[1,3],[2,6],[8,10],[15,18]]
Output: [[1,6],[8,10],[15,18]]
Explanation: Since intervals [1,3] and [2,6] overlaps, merge them into [1,6].

Example 2:
Input: intervals = [[1,4],[4,5]]
Output: [[1,5]]
Explanation: Intervals [1,4] and [4,5] are considered overlapping.


Constraints:

1 <= intervals.length <= 104
intervals[i].length == 2
0 <= starti <= endi <= 104
"""
from typing import List


# running time: O(nlogn) + O(n)
# space: O(n)

def merge(intervals: List[List[int]]) -> List[List[int]]:
    def overlap(interval1, interval2):
        interval1_end = interval1[1]
        interval2_start = interval2[0]
        if interval2_start <= interval1_end:
            return True
        return False

    merged_list = []
    intervals.sort(key=lambda x: x[0])
    merged_interval = None
    for i in range(0, len(intervals) - 1):
        if merged_interval is None:
            merged_interval = intervals[i]
        if overlap(merged_interval, intervals[i + 1]):
            merged_interval = [merged_interval[0], max(merged_interval[1], intervals[i + 1][1])]
        else:
            merged_list.append(merged_interval)
            merged_interval = None
    if merged_interval is None:
        merged_list.append(intervals[-1])
    else:
        merged_list.append(merged_interval)
    return merged_list

