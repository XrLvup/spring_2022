# Problem description:  Surrounded Regions
"""
Given an m x n matrix board containing 'X' and 'O', capture all regions that are 4-directionally surrounded by 'X'.

A region is captured by flipping all 'O's into 'X's in that surrounded region.



Example 1:

Input: board = [["X","X","X","X"],["X","O","O","X"],["X","X","O","X"],["X","O","X","X"]]
Output: [["X","X","X","X"],["X","X","X","X"],["X","X","X","X"],["X","O","X","X"]]
Explanation: Surrounded regions should not be on the border, which means that any 'O' on the border of the board are not flipped to 'X'. Any 'O' that is not on the border and it is not connected to an 'O' on the border will be flipped to 'X'. Two cells are connected if they are adjacent cells connected horizontally or vertically.

Example 2:
Input: board = [["X"]]
Output: [["X"]]


Constraints:

m == board.length
n == board[i].length
1 <= m, n <= 200
board[i][j] is 'X' or 'O'.
"""
from typing import List


# running time: O(mn)
# space: O(mn)

def solve(self, board: List[List[str]]) -> None:
    """
    Do not return anything, modify board in-place instead.
    """

    def get_neighbors(row: int, col: int) -> List[int]:
        return [(row + 1, col), (row, col + 1), (row - 1, col), (row, col - 1)]

    def isValidRow(row: int) -> bool:
        if row >= 0 and row < len(board):
            return True

    def isValidCol(col: int) -> bool:
        if col >= 0 and col < len(board[0]):
            return True

    def explore(row: int, col: int) -> None:
        board[row][col] = 'E'
        neighbors_list = get_neighbors(row, col)
        for n_row, n_col in neighbors_list:
            if isValidRow(n_row) and isValidCol(n_col) and board[n_row][n_col] == 'O':
                explore(n_row, n_col)

    not_captured_list = []
    # first column
    for i in range(0, len(board)):
        for j in range(0, len(board[0])):
            # explore only on boundary cells
            if (i == 0 or j == 0 or i == len(board) - 1 or j == len(board[0]) - 1) and board[i][j] == 'O':
                explore(i, j)
    # print(board)
    # now iterate through board marking E as O and O as X
    for i in range(0, len(board)):
        for j in range(0, len(board[0])):
            if board[i][j] == 'E':
                board[i][j] = 'O'
            elif board[i][j] == 'O':
                board[i][j] = 'X'

