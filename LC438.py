# Problem description:  Find All Anagrams in a String
"""
Given two strings s and p, return an array of all the start indices of p's anagrams in s.
You may return the answer in any order.

An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase,
typically using all the original letters exactly once.



Example 1:
Input: s = "cbaebabacd", p = "abc"
Output: [0,6]
Explanation:
The substring with start index = 0 is "cba", which is an anagram of "abc".
The substring with start index = 6 is "bac", which is an anagram of "abc".

Example 2:
Input: s = "abab", p = "ab"
Output: [0,1,2]
Explanation:
The substring with start index = 0 is "ab", which is an anagram of "ab".
The substring with start index = 1 is "ba", which is an anagram of "ab".
The substring with start index = 2 is "ab", which is an anagram of "ab".


Constraints:
1 <= s.length, p.length <= 3 * 104
s and p consist of lowercase English letters.

"""
from typing import List


# running time: O(n)
# space: O(n)

def findAnagrams(s: str, p: str) -> List[int]:
    if len(p) > len(s):
        return []
    p_dict = {}
    for p_ch in p:
        if p_ch not in p_dict:
            p_dict[p_ch] = 1
        else:
            p_dict[p_ch] += 1
    s_dict = {}
    result = []
    for i in range(0, len(p)):
        if s[i] not in s_dict:
            s_dict[s[i]] = 1
        else:
            s_dict[s[i]] += 1

    for i in range(len(p), len(s)):
        if s_dict == p_dict:
            result.append(i - len(p))
        if s[i] in s_dict:
            s_dict[s[i]] += 1
        else:
            s_dict[s[i]] = 1
        # remove entry
        if s_dict[s[i - len(p)]] == 1:
            del s_dict[s[i - len(p)]]
        else:
            s_dict[s[i - len(p)]] -= 1
    # print(s_dict)
    if s_dict == p_dict:
        result.append(len(s) - len(p))
    return result

