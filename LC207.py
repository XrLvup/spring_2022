# Problem description:   Course Schedule
"""
There are a total of numCourses courses you have to take, labeled from 0 to numCourses - 1. You are given an array prerequisites where prerequisites[i] = [ai, bi] indicates that you must take course bi first if you want to take course ai.

For example, the pair [0, 1], indicates that to take course 0 you have to first take course 1.
Return true if you can finish all courses. Otherwise, return false.



Example 1:
Input: numCourses = 2, prerequisites = [[1,0]]
Output: true
Explanation: There are a total of 2 courses to take.
To take course 1 you should have finished course 0. So it is possible.

Example 2:
Input: numCourses = 2, prerequisites = [[1,0],[0,1]]
Output: false
Explanation: There are a total of 2 courses to take.
To take course 1 you should have finished course 0, and to take course 0 you should also have finished course 1. So it is impossible.


Constraints:

1 <= numCourses <= 105
0 <= prerequisites.length <= 5000
prerequisites[i].length == 2
0 <= ai, bi < numCourses
All the pairs prerequisites[i] are unique.


Constraints:

1 <= target <= 109
1 <= nums.length <= 105
1 <= nums[i] <= 105


Follow up: If you have figured out the O(n) solution, try coding another solution of which the time complexity is O(n log(n)).
"""
from typing import List
import math


# Space: O(m+n)
# Time: O(m+n)

def canFinish(numCourses: int, prerequisites: List[List[int]]) -> bool:
    def build_adj_list() -> List[List[int]]:
        adj_list = []
        for i in range(numCourses):
            adj_list.append([])
        for dep in prerequisites:
            adj_list[dep[1]].append(dep[0])
        return adj_list

    def explore(visited: List[bool], adj_list: List[List[int]], course: int, post_order: List[int]) -> bool:
        visited[course] = True
        # print(f"course={course}")
        neighbors = adj_list[course]
        # print("here")
        for n in neighbors:
            if not visited[n]:
                status = explore(visited, adj_list, n, post_order)
                if not status:
                    return False
            else:
                if n not in post_order:
                    # print("here")
                    # print(course)
                    # print(post_order)
                    return False
                    # print(f"come here = {course}")
        post_order.add(course)
        # print(post_order)
        return True

    visited = [False] * numCourses
    post_order = set()
    adj_list = build_adj_list()
    # print(adj_list)
    for i in range(numCourses):
        if not visited[i]:
            status = explore(visited, adj_list, i, post_order)
            if not status:
                return False
    return True

