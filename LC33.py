# Problem description: Search in Rotated Sorted Array
"""
There is an integer array nums sorted in ascending order (with distinct values).

Prior to being passed to your function, nums is possibly rotated at an unknown pivot index k (1 <= k < nums.length) such that the resulting array is [nums[k], nums[k+1], ..., nums[n-1], nums[0], nums[1], ..., nums[k-1]] (0-indexed). For example, [0,1,2,4,5,6,7] might be rotated at pivot index 3 and become [4,5,6,7,0,1,2].

Given the array nums after the possible rotation and an integer target, return the index of target if it is in nums, or -1 if it is not in nums.
You must write an algorithm with O(log n) runtime complexity.



Example 1:

Input: nums = [4,5,6,7,0,1,2], target = 0
Output: 4
Example 2:

Input: nums = [4,5,6,7,0,1,2], target = 3
Output: -1
Example 3:

Input: nums = [1], target = 0
Output: -1
"""
from typing import List


# running time: O(nlogn)
# space: O(1)

def search(nums: List[int], target: int) -> int:
    # divide the rotate sorted array into 2 zones
    # and check if target belongs to which zone
    target_zone = None
    if target <= nums[-1]:
        target_zone = 2
    else:
        target_zone = 1
    start = 0
    end = len(nums) - 1
    while start <= end:
        mid = start + (end - start) // 2
        if nums[mid] == target:  # best case
            return mid
        # check the zone if it belongs to 2
        if nums[mid] <= nums[-1] and target_zone == 1:
            end = mid - 1
        elif nums[mid] > nums[-1] and target_zone == 2:
            start = mid + 1
        else:  # reg binary search
            if nums[mid] < target:
                start = mid + 1
            else:
                end = mid - 1
    return -1
