# Problem description:  Guess Number Higher or Lower
"""
We are playing the Guess Game. The game is as follows:

I pick a number from 1 to n. You have to guess which number I picked.

Every time you guess wrong, I will tell you whether the number I picked is higher or lower than your guess.

You call a pre-defined API int guess(int num), which returns 3 possible results:

-1: The number I picked is lower than your guess (i.e. pick < num).
1: The number I picked is higher than your guess (i.e. pick > num).
0: The number I picked is equal to your guess (i.e. pick == num).
Return the number that I picked.



Example 1:

Input: n = 10, pick = 6
Output: 6
Example 2:

Input: n = 1, pick = 1
Output: 1
Example 3:

Input: n = 2, pick = 1
Output: 1


Constraints:

1 <= n <= 231 - 1
1 <= pick <= n
"""
from typing import List


# running time: O(log(n))
# space: O(1)

# The guess API is already defined for you.
# @param num, your guess
# @return -1 if my number is lower, 1 if my number is higher, otherwise return 0
def guess(num: int) -> int:

def guessNumber(n: int) -> int:
    start = 1
    end = n
    while start <= end:
        mid = start + (end - start) // 2
        value = guess(mid)
        if value == 0:
            return mid
        elif value == 1:  # target > mid
            start = mid + 1
        else:
            end = mid - 1
    return -1  # should not come here

