# Problem description:  Remove Duplicates from Sorted List
"""
Given the head of a sorted linked list, delete all duplicates such that each element appears only once. Return the linked list sorted as well.

Example 1:
Input: head = [1,1,2]
Output: [1,2]

Example 2:
Input: head = [1,1,2,3,3]
Output: [1,2,3]

Constraints:
The number of nodes in the list is in the range [0, 300].
-100 <= Node.val <= 100
The list is guaranteed to be sorted in ascending order.
"""
from typing import List, Optional


# running time: O(n)
# space: O(1)

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

# Space: O(1)
# Time :O(n)
def deleteDuplicates(head: Optional[ListNode]) -> Optional[ListNode]:
    sentinel = ListNode(-1)
    sentinel.next = head
    current = sentinel.next
    while current is not None and current.next is not None:
        if current.val != current.next.val:
            current = current.next
        else:
            current.next = current.next.next
    return sentinel.next
