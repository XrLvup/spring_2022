# Problem description:  Subarray Sum Equals K
"""
Given an array of integers nums and an integer k, return the total number of continuous subarrays whose sum equals to k.



Example 1:

Input: nums = [1,1,1], k = 2
Output: 2

Example 2:
Input: nums = [1,2,3], k = 3
Output: 2


Constraints:

1 <= nums.length <= 2 * 104
-1000 <= nums[i] <= 1000
-107 <= k <= 107
"""

# running time: O(n)
# space: O(n)
from typing import List


def subarraySum(nums: List[int], k: int) -> int:
    # key = prefix_sum_count , value = no of subarrays adding to it
    hmap = {0: 1}
    global_count = 0
    prefix_sum = 0
    for i in range(0, len(nums)):
        # compute prefix_sum[i]
        prefix_sum += nums[i]
        # compute subarray sum ending at i
        if prefix_sum - k in hmap:
            # print("here")
            global_count += hmap[prefix_sum - k]
        # update my entry in hmap
        if prefix_sum in hmap:
            hmap[prefix_sum] += 1
        else:
            hmap[prefix_sum] = 1
    return global_count
