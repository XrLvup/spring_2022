# Problem description:  Contiguous Array
"""
Given a binary array nums, return the maximum length of a contiguous subarray with an equal number of 0 and 1.



Example 1:
Input: nums = [0,1]
Output: 2
Explanation: [0, 1] is the longest contiguous subarray with an equal number of 0 and 1.

Example 2:
Input: nums = [0,1,0]
Output: 2
Explanation: [0, 1] (or [1, 0]) is a longest contiguous subarray with equal number of 0 and 1.


Constraints:

1 <= nums.length <= 105
nums[i] is either 0 or 1.
"""

# running time: O(n)
# space: O(n)
from typing import List


def findMaxLength(nums: List[int]) -> int:
    prefix_excess = 0
    global_max = 0
    hmap = {0: 0}  # account for empty array
    for i in range(0, len(nums)):
        if nums[i] == 1:
            prefix_excess += 1
        else:
            prefix_excess -= 1
        if prefix_excess in hmap:
            global_max = max(global_max, i + 1 - hmap[prefix_excess])
        if prefix_excess not in hmap:
            hmap[prefix_excess] = i + 1
    return global_max
