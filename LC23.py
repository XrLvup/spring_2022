# Problem description:   Merge k Sorted Lists
"""
You are given an array of k linked-lists lists, each linked-list is sorted in ascending order.

Merge all the linked-lists into one sorted linked-list and return it.



Example 1:

Input: lists = [[1,4,5],[1,3,4],[2,6]]
Output: [1,1,2,3,4,4,5,6]
Explanation: The linked-lists are:
[
  1->4->5,
  1->3->4,
  2->6
]
merging them into one sorted list:
1->1->2->3->4->4->5->6

Example 2:
Input: lists = []
Output: []

Example 3:
Input: lists = [[]]
Output: []


Constraints:

k == lists.length
0 <= k <= 10^4
0 <= lists[i].length <= 500
-10^4 <= lists[i][j] <= 10^4
lists[i] is sorted in ascending order.
The sum of lists[i].length won't exceed 10^4.
"""
from typing import List
import heapq


# running time: O(Nlogk) where N is total no of elements in final list
# space: O(1)

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


def mergeKLists(lists):
    """
    :type lists: List[ListNode]
    :rtype: ListNode
    """
    min_heap = []
    # insert k elements onto the heap
    for i in range(0, len(lists)):
        if lists[i] is not None:
            min_heap.append((lists[i].val, lists[i]))
            # heapq.heappush(min_heap, (lists[i].val, lists[i]))
    # print(min_heap)
    # now build heap
    heapq.heapify(min_heap)
    sentinel = ListNode(-1)
    prev = sentinel
    while min_heap:
        (val, node) = heapq.heappop(min_heap)
        prev.next = node
        node = node.next
        if node is not None:
            heapq.heappush(min_heap, (node.val, node))
        prev = prev.next
    return sentinel.next
