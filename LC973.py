# Problem description:  K Closest Points to Origin
"""
Given an array of points where points[i] = [xi, yi] represents a point on the X-Y plane and an integer k, return the k closest points to the origin (0, 0).

The distance between two points on the X-Y plane is the Euclidean distance (i.e., √(x1 - x2)2 + (y1 - y2)2).

You may return the answer in any order. The answer is guaranteed to be unique (except for the order that it is in).


Example 1:
Input: points = [[1,3],[-2,2]], k = 1
Output: [[-2,2]]
Explanation:
The distance between (1, 3) and the origin is sqrt(10).
The distance between (-2, 2) and the origin is sqrt(8).
Since sqrt(8) < sqrt(10), (-2, 2) is closer to the origin.
We only want the closest k = 1 points from the origin, so the answer is just [[-2,2]].

Example 2:
Input: points = [[3,3],[5,-1],[-2,4]], k = 2
Output: [[3,3],[-2,4]]
Explanation: The answer [[-2,4],[3,3]] would also be accepted.


Constraints:

1 <= k <= points.length <= 104
-104 < xi, yi < 104
"""
from typing import List
import random


# running time: O(n)
# space: O(1)

def findKthLargest(nums: List[int], k: int) -> int:
    def quick_select(start: int, end: int, k: int) -> int:
        # base case
        if start >= end:  #
            return start
        # find the pivot element - pick random
        pivot = random.randint(start, end)
        # swap with first element
        nums[start], nums[pivot] = nums[pivot], nums[start]
        index = partition(start, end)
        if index == k:
            return index
        if index < k:
            return quick_select(index + 1, end, k)
        # index > k
        return quick_select(start, index - 1, k)

    def partition(start: int, end: int) -> int:
        pivot = nums[start]
        lt = start
        for bigger in range(start + 1, end + 1):
            if nums[bigger] <= pivot:
                lt += 1
                nums[bigger], nums[lt] = nums[lt], nums[bigger]
        nums[lt], nums[start] = nums[start], nums[lt]
        return lt

    n = len(nums)
    index = quick_select(0, n - 1, n - k)
    # print(index)
    # print(nums)
    return nums[index]
