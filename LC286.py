# Problem description:  Walls and Gates
"""
You are given an m x n grid rooms initialized with these three possible values.

-1 A wall or an obstacle.
0 A gate.
INF Infinity means an empty room. We use the value 231 - 1 = 2147483647 to represent INF as you may assume that the distance to a gate is less than 2147483647.
Fill each empty room with the distance to its nearest gate. If it is impossible to reach a gate, it should be filled with INF.


Example 1:
Input: rooms = [[2147483647,-1,0,2147483647],[2147483647,2147483647,2147483647,-1],[2147483647,-1,2147483647,-1],[0,-1,2147483647,2147483647]]
Output: [[3,-1,0,1],[2,2,1,-1],[1,-1,2,-1],[0,-1,3,4]]

Example 2:
Input: rooms = [[-1]]
Output: [[-1]]


Constraints:

m == rooms.length
n == rooms[i].length
1 <= m, n <= 250
rooms[i][j] is -1, 0, or 231 - 1
"""
from typing import List
from collections import deque

# running time: O(mn)
# space: O(mn)


def wallsAndGates(rooms: List[List[int]]) -> None:
    """
    Do not return anything, modify rooms in-place instead.
    """

    def get_neighbors(room_row: int, room_col: int) -> List[int]:
        return [(room_row + 1, room_col), (room_row - 1, room_col), (room_row, room_col + 1), (room_row, room_col - 1)]

    def isvalidRow(room_row: int) -> bool:
        if room_row >= 0 and room_row <= len(rooms) - 1:
            return True

    def isvalidCol(room_col: int) -> bool:
        if room_col >= 0 and room_col <= len(rooms[0]) - 1:
            return True

    def bfs(start_list) -> None:
        queue = deque([])
        visited_set = set()
        for room_row, room_col in start_list:
            visited_set.add((room_row, room_col))
            queue.append((room_row, room_col, 0))
        while queue:
            (row, col, dist) = queue.popleft()
            # print(f"Row = {row}, Col={col}, Dist ={dist}")
            neighbors = get_neighbors(row, col)
            for n_row, n_col in neighbors:
                if isvalidRow(n_row) and isvalidCol(n_col) and rooms[n_row][n_col] != -1 and (
                n_row, n_col) not in visited_set:
                    visited_set.add((n_row, n_col))
                    queue.append((n_row, n_col, dist + 1))
                    rooms[n_row][n_col] = dist + 1

    start_list = []
    for row in range(0, len(rooms)):
        for col in range(0, len(rooms[0])):
            if rooms[row][col] == 0:
                start_list.append((row, col))
    # print(start_list)
    bfs(start_list)

