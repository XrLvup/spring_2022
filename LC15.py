# Problem description:  3Sum
"""
Given an integer array nums, return all the triplets [nums[i], nums[j], nums[k]] such that i != j, i != k, and j != k, and nums[i] + nums[j] + nums[k] == 0.

Notice that the solution set must not contain duplicate triplets.



Example 1:
Input: nums = [-1,0,1,2,-1,-4]
Output: [[-1,-1,2],[-1,0,1]]

Example 2:
Input: nums = []
Output: []

Example 3:
Input: nums = [0]
Output: []


Constraints:

0 <= nums.length <= 3000
-105 <= nums[i] <= 105
"""

from typing import List


# running time: O(n^2)
# space: O(n)

def threeSum(nums: List[int]) -> List[List[int]]:
    def twoSum(index: int, result: List[int]) -> None:
        target = -1 * nums[index]
        i = index + 1
        j = len(nums) - 1
        while i < j:
            if nums[i] + nums[j] == target:
                triplet = [nums[index], nums[i], nums[j]]
                # double check for dups
                if not result:
                    result.append(triplet)
                elif result and result[-1] != triplet:
                    result.append(triplet)
                # can have more than 1 copy
                i += 1
                j -= 1
            elif nums[i] + nums[j] < target:
                i += 1
            else:
                j -= 1

    # sort the nums array
    nums.sort()
    result = []
    for i in range(0, len(nums)):
        target_val = -1 * nums[i]
        # check for dups
        if i == 0 or nums[i] != nums[i - 1]:
            twoSum(i, result)
    return result
