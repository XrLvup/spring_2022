# Problem description: Sliding Window Maximum
"""
You are given an array of integers nums, there is a sliding window of size k which is moving from the very left of the array to the very right. You can only see the k numbers in the window. Each time the sliding window moves right by one position.

Return the max sliding window.



Example 1:

Input: nums = [1,3,-1,-3,5,3,6,7], k = 3
Output: [3,3,5,5,6,7]
Explanation:
Window position                Max
---------------               -----
[1  3  -1] -3  5  3  6  7       3
 1 [3  -1  -3] 5  3  6  7       3
 1  3 [-1  -3  5] 3  6  7       5
 1  3  -1 [-3  5  3] 6  7       5
 1  3  -1  -3 [5  3  6] 7       6
 1  3  -1  -3  5 [3  6  7]      7

Example 2:
Input: nums = [1], k = 1
Output: [1]


Constraints:

1 <= nums.length <= 105
-104 <= nums[i] <= 104
1 <= k <= nums.length
"""
from typing import List
from collections import deque


# m = len(s) and n = len(t)
# Running time: O(n)
# Space: O(k) - could be stated as constant O(1) since m, n <= 26
def maxSlidingWindow(nums: List[int], k: int) -> List[int]:
    # keep track of elements in decreasing order
    queue = deque([])
    output_list = []
    # initialization phase
    for i in range(0, k):
        while queue and queue[-1] < nums[i]:
            # then queue[-1] cannot be biggest value
            queue.pop()
        queue.append(nums[i])
    # print(queue)
    output_list.append(queue[0])
    # rest phase
    for i in range(k, len(nums)):
        # remove the leftmost number if max
        if nums[i - k] == queue[0]:
            queue.popleft()
        # check whether to add nums[i] to the sliding window
        while queue and queue[-1] < nums[i]:
            # then queue[-1] cannot be biggest value
            queue.pop()
        queue.append(nums[i])
        output_list.append(queue[0])

    return output_list
