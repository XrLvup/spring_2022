# Problem description:  Number of Connected Components in an Undirected Graph
"""
You have a graph of n nodes. You are given an integer n and an array edges where edges[i] = [ai, bi] indicates that there is an edge between ai and bi in the graph.

Return the number of connected components in the graph.


Example 1:
Input: n = 5, edges = [[0,1],[1,2],[3,4]]
Output: 2

Example 2:
Input: n = 5, edges = [[0,1],[1,2],[2,3],[3,4]]
Output: 1


Constraints:

1 <= n <= 2000
1 <= edges.length <= 5000
edges[i].length == 2
0 <= ai <= bi < n
ai != bi
There are no repeated edges.
"""

# Space = O(n+m) - for adj list, O(n) - for visited, O(n)- dfs call stack
# Time = O(n+m) - run dfs routine
from typing import List


def countComponents(n: int, edges: List[List[int]]) -> int:
    def buildAdjMap() -> dict:
        hmap = {}
        for edge in edges:
            first = edge[0]
            second = edge[1]
            if first not in hmap:
                hmap[first] = [second]
            else:
                hmap[first].append(second)
            if second not in hmap:
                hmap[second] = [first]
            else:
                hmap[second].append(first)
        return hmap

    def explore(visited: List[bool], hmap: dict, node: int) -> None:
        visited[node] = True
        neighbors = []
        if node in hmap:
            neighbors = hmap[node]
        for n in neighbors:
            if not visited[n]:
                explore(visited, hmap, n)

    visited = [False] * n
    hmap = buildAdjMap()
    # print(hmap)
    cc = 0
    for key in range(n):
        if not visited[key]:
            explore(visited, hmap, key)
            cc += 1
    return cc

