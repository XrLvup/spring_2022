# Problem description:  Kth Largest Element in an Array
"""
Given an integer array nums and an integer k, return the kth largest element in the array.

Note that it is the kth largest element in the sorted order, not the kth distinct element.



Example 1:

Input: nums = [3,2,1,5,6,4], k = 2
Output: 5
Example 2:

Input: nums = [3,2,3,1,2,4,5,5,6], k = 4
Output: 4


Constraints:

1 <= k <= nums.length <= 104
-104 <= nums[i] <= 104
"""
from typing import List
import random


# running time: O(n)
# space: O(1)

def findKthLargest(self, nums: List[int], k: int) -> int:
    def quick_select(start: int, end: int, k: int) -> int:
        # base case
        if start >= end:  #
            return start
        # find the pivot element - pick random
        pivot = random.randint(start, end)
        # swap with first element
        nums[start], nums[pivot] = nums[pivot], nums[start]
        index = partition(start, end)
        if index == k:
            return index
        if index < k:
            return quick_select(index + 1, end, k)
        # index > k
        return quick_select(start, index - 1, k)

    def partition(start: int, end: int) -> int:
        pivot = nums[start]
        lt = start
        for bigger in range(start + 1, end + 1):
            if nums[bigger] <= pivot:
                lt += 1
                nums[bigger], nums[lt] = nums[lt], nums[bigger]
        nums[lt], nums[start] = nums[start], nums[lt]
        return lt

    n = len(nums)
    index = quick_select(0, n - 1, n - k)
    # print(index)
    # print(nums)
    return nums[index]
