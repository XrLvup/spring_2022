# Problem description:  Check If a Number Is Majority Element in a Sorted Array
"""
Given an integer array nums sorted in non-decreasing order and an integer target, return true if target is a majority element, or false otherwise.

A majority element in an array nums is an element that appears more than nums.length / 2 times in the array.



Example 1:

Input: nums = [2,4,5,5,5,5,5,6,6], target = 5
Output: true
Explanation: The value 5 appears 5 times and the length of the array is 9.
Thus, 5 is a majority element because 5 > 9/2 is true.

Example 2:
Input: nums = [10,100,101,101], target = 101
Output: false
Explanation: The value 101 appears 2 times and the length of the array is 4.
Thus, 101 is not a majority element because 2 > 4/2 is false.


Constraints:

1 <= nums.length <= 1000
1 <= nums[i], target <= 109
nums is sorted in non-decreasing order.
"""
from typing import List

# running time: O(log(n))
# space: O(1)

def isMajorityElement(nums: List[int], target: int) -> bool:
    # 2 binary searches
    # first search to find start position of target
    # second search to find last position of target
    # difference has to greater than n/2 to be majority element

    # now finding start position of target
    start = 0
    end = len(nums) - 1
    while start <= end:
        mid = start + (end - start) // 2
        if nums[mid] < target:
            start = mid + 1
        else:
            end = mid - 1
    if start == -1:
        return False
    # print(start)
    first = start
    # now finding last position of target
    end = len(nums) - 1
    while start <= end:
        mid = start + (end - start) // 2
        if nums[mid] <= target:
            start = mid + 1
        else:
            end = mid - 1
    # print(end)
    if end - first + 1 > len(nums) // 2:
        return True
    return False

