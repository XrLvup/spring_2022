# Problem description:  Maximum Number of Vowels in a Substring of Given Length
"""
Given a string s and an integer k, return the maximum number of vowel letters in any substring of s with length k.

Vowel letters in English are 'a', 'e', 'i', 'o', and 'u'.



Example 1:
Input: s = "abciiidef", k = 3
Output: 3
Explanation: The substring "iii" contains 3 vowel letters.

Example 2:
Input: s = "aeiou", k = 2
Output: 2
Explanation: Any substring of length 2 contains 2 vowels.

Example 3:
Input: s = "leetcode", k = 3
Output: 2
Explanation: "lee", "eet" and "ode" contain 2 vowels.


Constraints:

1 <= s.length <= 105
s consists of lowercase English letters.
1 <= k <= s.length
"""
from typing import List
import random


# running time: O(n)
# space: O(1)

def maxVowels(s: str, k: int) -> int:
    vowel_set = set(['a', 'e', 'i', 'o', 'u'])
    cur_size = 0
    # first sliding window
    for i in range(0, k):
        if s[i] in vowel_set:
            cur_size += 1
    max_size = cur_size
    for i in range(k, len(s)):
        if s[i] in vowel_set:
            cur_size += 1
        if s[i - k] in vowel_set:
            cur_size -= 1
        max_size = max(max_size, cur_size)
    return max_size
