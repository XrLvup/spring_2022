# Problem description:  Valid Anagram
"""
Given two strings s and t, return true if t is an anagram of s, and false otherwise.

An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase, typically using all the original letters exactly once.



Example 1:

Input: s = "anagram", t = "nagaram"
Output: true
Example 2:

Input: s = "rat", t = "car"
Output: false


Constraints:

1 <= s.length, t.length <= 5 * 104
s and t consist of lowercase English letters.


Follow up: What if the inputs contain Unicode characters? How would you adapt your solution to such a case?
"""
from typing import List

# m = len(s) and n = len(t)
# Running time: O(m+n)
# Space: O(1) - could be stated as constant O(1) since m, n <= 26
def isAnagram(s: str, t: str) -> bool:
    s_dict = {}
    for s_ch in s:
        if s_ch not in s_dict:
            s_dict[s_ch] = 1
        else:
            s_dict[s_ch] += 1
    t_dict = {}
    for t_ch in t:
        if t_ch not in t_dict:
            t_dict[t_ch] = 1
        else:
            t_dict[t_ch] += 1
    return s_dict == t_dict

