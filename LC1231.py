# Problem description:  Divide Chocolate
"""
You have one chocolate bar that consists of some chunks. Each chunk has its own sweetness given by the array sweetness.

You want to share the chocolate with your k friends so you start cutting the chocolate bar into k + 1 pieces using k cuts, each piece consists of some consecutive chunks.

Being generous, you will eat the piece with the minimum total sweetness and give the other pieces to your friends.

Find the maximum total sweetness of the piece you can get by cutting the chocolate bar optimally.



Example 1:

Input: sweetness = [1,2,3,4,5,6,7,8,9], k = 5
Output: 6
Explanation: You can divide the chocolate to [1,2,3], [4,5], [6], [7], [8], [9]

Example 2:
Input: sweetness = [5,6,7,8,9,1,2,3,4], k = 8
Output: 1
Explanation: There is only one way to cut the bar into 9 pieces.

Example 3:
Input: sweetness = [1,2,2,1,2,2,1,2,2], k = 2
Output: 5
Explanation: You can divide the chocolate to [1,2,2], [1,2,2], [1,2,2]


Constraints:

0 <= k < sweetness.length <= 104
1 <= sweetness[i] <= 105
"""
from typing import List
import random


# running time: O(n)
# space: O(1)

def maximizeSweetness(self, sweetness: List[int], k: int) -> int:
    # is is possible to break the bar into k+1 pieces in which each
    # piece has min sweetness atleast sweet_val
    def is_possible(sweet_val: int) -> int:
        pieces = 0
        val = 0
        for i in range(0, len(sweetness)):
            val += sweetness[i]
            if val >= sweet_val:
                pieces += 1
                val = 0
        return pieces > k

    # sweetness range from min_val in sweetness to to all the values in sweetness
    start = min(sweetness)  # k = len(sweetness)-1
    end = sum(sweetness)  # k = 0, pieces = 1
    while start <= end:
        mid = start + (end - start) // 2
        # print(f"start={start}, end={end}, mid={mid}")
        if is_possible(mid):
            start = mid + 1
        else:
            end = mid - 1
    return end
