# Problem description:  Number of Provinces
"""
There are n cities. Some of them are connected, while some are not. If city a is connected directly with city b, and city b is connected directly with city c, then city a is connected indirectly with city c.

A province is a group of directly or indirectly connected cities and no other cities outside of the group.

You are given an n x n matrix isConnected where isConnected[i][j] = 1 if the ith city and the jth city are directly connected, and isConnected[i][j] = 0 otherwise.

Return the total number of provinces.



Example 1:
Input: isConnected = [[1,1,0],[1,1,0],[0,0,1]]
Output: 2

Example 2:
Input: isConnected = [[1,0,0],[0,1,0],[0,0,1]]
Output: 3
"""

# running time: O(n)
# space: O(k)
from typing import List


def findCircleNum(isConnected: List[List[int]]) -> int:
    def explore(visited: List[bool], city: int) -> None:
        visited[city] = True
        neighbors = isConnected[city]
        for n in range(0, len(neighbors)):
            if n != city and not visited[n] and isConnected[city][n]:
                explore(visited, n)

    # need to keep track of nodes visited
    visited = [False] * len(isConnected)
    cc = 0
    for city in range(0, len(isConnected)):
        if not visited[city]:
            explore(visited, city)
            cc += 1
    return cc
