# Problem description:  Max Consecutive Ones III
"""
Given a binary array nums and an integer k, return the maximum number of consecutive 1's in the array if you can flip at most k 0's.



Example 1:

Input: nums = [1,1,1,0,0,0,1,1,1,1,0], k = 2
Output: 6
Explanation: [1,1,1,0,0,1,1,1,1,1,1]
Bolded numbers were flipped from 0 to 1. The longest subarray is underlined.

Example 2:
Input: nums = [0,0,1,1,0,0,1,1,1,0,1,1,0,0,0,1,1,1,1], k = 3
Output: 10
Explanation: [0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,1,1,1,1]
Bolded numbers were flipped from 0 to 1. The longest subarray is underlined.


Constraints:

1 <= nums.length <= 105
nums[i] is either 0 or 1.
0 <= k <= nums.length
"""
from typing import List



# running time: O(n)
# space: O(1)

def longestOnes(nums: List[int], k: int) -> int:
    global_max = 0
    left = 0
    k_counter = 0
    for i in range(0, len(nums)):
        if nums[i] == 0:
            k_counter += 1

        while left <= i and k_counter > k:
            if nums[left] == 0:
                k_counter -= 1
            left += 1

        if k_counter <= k:
            global_max = max(global_max, i - left + 1)
    return global_max



