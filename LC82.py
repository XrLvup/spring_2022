# Problem description:  Remove Duplicates from Sorted List II
"""
Given the head of a sorted linked list, delete all nodes that have duplicate numbers, leaving only distinct numbers from the original list.
Return the linked list sorted as well.

Example 1:
Input: head = [1,2,3,3,4,4,5]
Output: [1,2,5]

Example 2:
Input: head = [1,1,1,2,3]
Output: [2,3]

Constraints:
The number of nodes in the list is in the range [0, 300].
-100 <= Node.val <= 100
The list is guaranteed to be sorted in ascending order.
"""
from typing import List, Optional


# running time: O(n)
# space: O(1)

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


# Time :O(n)
# Space: O(1)

def deleteDuplicates(head: Optional[ListNode]) -> Optional[ListNode]:
    sentinel = ListNode(-1)
    prev = sentinel
    prev.next = head
    current = head
    while current is not None and current.next is not None:
        if current.val != current.next.val:
            prev = current
            current = current.next
        else:
            value = current.val
            while current is not None and current.val == value:
                current = current.next
            prev.next = current
    return sentinel.next
