# Problem description: Minimum Operations to Reduce X to Zero
"""
You are given an integer array nums and an integer x. In one operation, you can either remove the leftmost or the rightmost element from the array nums and subtract its value from x. Note that this modifies the array for future operations.

Return the minimum number of operations to reduce x to exactly 0 if it is possible, otherwise, return -1.



Example 1:

Input: nums = [1,1,4,2,3], x = 5
Output: 2
Explanation: The optimal solution is to remove the last two elements to reduce x to zero.

Example 2:
Input: nums = [5,6,7,8,9], x = 4
Output: -1

Example 3:
Input: nums = [3,2,20,1,1,3], x = 10
Output: 5
Explanation: The optimal solution is to remove the last three elements and the first two elements (5 operations in total) to reduce x to zero.


Constraints:

1 <= nums.length <= 105
1 <= nums[i] <= 104
1 <= x <= 109
"""
from typing import List
import random


# running time: O(n)
# space: O(1)

def minOperations(nums: List[int], x: int) -> int:
    global_min_operations = len(nums) + 1
    k = sum(nums) - x
    left = 0
    window_sum = 0
    # reducing problem to find the max subarray whose sum = k
    for i in range(0, len(nums)):
        window_sum += nums[i]
        while left <= i and window_sum > k:
            window_sum -= nums[left]
            left += 1
        if window_sum == k:
            sub_array_size = i - left + 1
            operations = len(nums) - sub_array_size
            global_min_operations = min(operations, global_min_operations)
    if global_min_operations == len(nums) + 1:
        return -1
    return global_min_operations
