# Problem description:   Maximum Size Subarray Sum Equals k
"""
Given an integer array nums and an integer k, return the maximum length of a subarray that sums to k. If there is not one, return 0 instead.



Example 1:

Input: nums = [1,-1,5,-2,3], k = 3
Output: 4
Explanation: The subarray [1, -1, 5, -2] sums to 3 and is the longest.

Example 2:
Input: nums = [-2,-1,2,1], k = 1
Output: 2
Explanation: The subarray [-1, 2] sums to 1 and is the longest.


Constraints:

1 <= nums.length <= 2 * 105
-104 <= nums[i] <= 104
-109 <= k <= 109
"""


# Running time: O(n) -
# Space: O(k)
from typing import List


def maxSubArrayLen(nums: List[int], k: int) -> int:
    # key = prefix_sum, val = min_length
    # to get suffixes of max_length, prefixes should be of smallest value
    hmap = {0: 0}
    prefix_sum = 0
    max_length = 0
    for i in range(0, len(nums)):
        prefix_sum += nums[i]
        if prefix_sum - k in hmap:
            max_length = max(max_length, i + 1 - hmap[prefix_sum - k])

        if prefix_sum not in hmap:
            hmap[prefix_sum] = i + 1
        # else no need to update since smaller value already stored
    return max_length

