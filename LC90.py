# Problem description:  Subsets II
"""
Given an integer array nums that may contain duplicates, return all possible subsets (the power set).

The solution set must not contain duplicate subsets. Return the solution in any order.



Example 1:
Input: nums = [1,2,2]
Output: [[],[1],[1,2],[1,2,2],[2],[2,2]]

Example 2:
Input: nums = [0]
Output: [[],[0]]


Constraints:

1 <= nums.length <= 10
-10 <= nums[i] <= 10
"""
from typing import List


# Time: nO(2^n)
# Space: nO(2^n)
def subsetsWithDup(nums: List[int]) -> List[List[int]]:
    def helper(cur_index: int, slate: List[int]):
        if cur_index >= len(nums):
            result.append(slate[:])
            return
        no_copies = 1
        for i in range(cur_index, len(nums) - 1):
            if nums[i] == nums[i + 1]:
                no_copies += 1
            else:
                break
        # inclusion step
        for i in range(0, no_copies):
            slate.append(nums[cur_index])
            helper(cur_index + no_copies, slate)
        for i in range(0, no_copies):
            slate.pop()
        # exclusion step
        helper(cur_index + no_copies, slate)

    # sort the input
    nums.sort()
    # holding output
    result = []
    # call
    helper(0, [])
    return result
