# Problem description:  Find Peak Element
"""
A peak element is an element that is strictly greater than its neighbors.

Given an integer array nums, find a peak element, and return its index. If the array contains multiple peaks, return the index to any of the peaks.

You may imagine that nums[-1] = nums[n] = -∞.

You must write an algorithm that runs in O(log n) time.

Example 1:

Input: nums = [1,2,3,1]
Output: 2
Explanation: 3 is a peak element and your function should return the index number 2.
Example 2:

Input: nums = [1,2,1,3,5,6,4]
Output: 5
Explanation: Your function can return either index number 1 where the peak element is 2, or index number 5 where the peak element is 6.


Constraints:

1 <= nums.length <= 1000
-231 <= nums[i] <= 231 - 1
nums[i] != nums[i + 1] for all valid i.
"""
from typing import List


# running time: O(log(n))
# space: O(1)

def findPeakElement(nums: List[int]) -> int:
    # binary search as sequence of zones
    # edge case when there are less than or equal to 2 elements can be handled separately
    if len(nums) == 1:
        return 0
    if len(nums) == 2:
        if nums[0] > nums[1]:
            return 0
        else:
            return 1
    start = 0
    end = len(nums) - 1
    while start <= end:
        mid = start + (end - start) // 2
        # dealing with mid being last element
        if mid == len(nums) - 1 and nums[mid] > nums[mid - 1]:
            return mid
        # dealing with mid being first element
        if mid == 0 and nums[mid] > nums[mid + 1]:
            return mid
        # compare with neighbors to find peak
        if nums[mid] > nums[mid - 1] and nums[mid] > nums[mid + 1]:
            return mid
        elif nums[mid] < nums[mid + 1]:
            # part of ascending zone
            # potential for mid+1 to be peak
            start = mid + 1
        else:
            # part of descending zone, nums[mid] >= nums[mid+1]
            end = mid - 1
        # case of valley , could either chose start = mid + 1 or end = mid -1
    # should never come here
    return -1

