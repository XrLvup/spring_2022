# Problem description:   Graph Valid Tree
"""
You have a graph of n nodes labeled from 0 to n - 1. You are given an integer n and a list of edges where edges[i] = [ai, bi] indicates that there is an undirected edge between nodes ai and bi in the graph.

Return true if the edges of the given graph make up a valid tree, and false otherwise.


Example 1:
Input: n = 5, edges = [[0,1],[0,2],[0,3],[1,4]]
Output: true

Example 2:
Input: n = 5, edges = [[0,1],[1,2],[2,3],[1,3],[1,4]]
Output: false


Constraints:
1 <= n <= 2000
0 <= edges.length <= 5000
edges[i].length == 2
0 <= ai, bi < n
ai != bi
There are no self-loops or repeated edges.
"""
from typing import List


# Space = O(n+m)
# Time = O(n)

def validTree(n: int, edges: List[List[int]]) -> bool:
    def explore(hmap: dict, visited: List[bool], vertex: int, parent: int) -> bool:
        visited[vertex] = True
        if vertex not in hmap:
            # indicate more one than 1 connected component
            return False
        neighbors = hmap[vertex]
        for n in neighbors:
            if not visited[n]:
                status = explore(hmap, visited, n, vertex)
                if not status:
                    return False
            else:  # node is visited - to identify back edge
                if n != parent:
                    # print("here")
                    return False
        return True

    def buildhmap() -> dict:
        hmap = {}
        for edge in edges:
            first = edge[0]
            second = edge[1]
            if first not in hmap:
                hmap[first] = [second]
            else:
                hmap[first].append(second)
            if second not in hmap:
                hmap[second] = [first]
            else:
                hmap[second].append(first)
        return hmap

    visited = [False] * n
    hmap = buildhmap()
    if not edges and n == 1:
        return True
    for i in range(n):
        if i > 0 and not visited[i]:
            return False
        if not visited[i]:
            status = explore(hmap, visited, i, -1)
            if not status:
                return False
    return True

