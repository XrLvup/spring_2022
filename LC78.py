# Problem description:  Subsets
"""
Given an integer array nums of unique elements, return all possible subsets (the power set).

The solution set must not contain duplicate subsets. Return the solution in any order.



Example 1:

Input: nums = [1,2,3]
Output: [[],[1],[2],[1,2],[3],[1,3],[2,3],[1,2,3]]

Example 2:
Input: nums = [0]
Output: [[],[0]]


Constraints:

1 <= nums.length <= 10
-10 <= nums[i] <= 10
All the numbers of nums are unique.
"""
from typing import List


# Time = n2^n
# space = n2^n
# 2^n subsets are generated with each occupying n space
def subsets(nums: List[int]) -> List[List[int]]:
    def helper(cur_index, partial_soln):
        # base case
        if cur_index == len(nums):
            # O(n) work
            result.append(partial_soln[:])
            return
        # 2 recursive calls at each step
        # inclusion of nums[cur_index]
        partial_soln.append(nums[cur_index])
        helper(cur_index + 1, partial_soln)
        partial_soln.pop()
        # exclusion of nums[cur_index]
        helper(cur_index + 1, partial_soln)

    result = []
    helper(0, [])
    return result

