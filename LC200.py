# Problem description:  Number of Islands
"""
Given an m x n 2D binary grid grid which represents a map of '1's (land) and '0's (water), return the number of islands.

An island is surrounded by water and is formed by connecting adjacent lands horizontally or vertically. You may assume all four edges of the grid are all surrounded by water.



Example 1:

Input: grid = [
  ["1","1","1","1","0"],
  ["1","1","0","1","0"],
  ["1","1","0","0","0"],
  ["0","0","0","0","0"]
]
Output: 1

Example 2:
Input: grid = [
  ["1","1","0","0","0"],
  ["1","1","0","0","0"],
  ["0","0","1","0","0"],
  ["0","0","0","1","1"]
]
Output: 3
"""
from typing import List


# Time = O(mn)
# Space = O(mn)

def numIslands(grid: List[List[str]]) -> int:
    def explore(row: int, col: int) -> None:
        grid[row][col] = "0"  # mark as visited
        for n_row, n_col in get_neighbors(row, col):
            explore(n_row, n_col)

    def get_neighbors(row: int, col: int) -> List[int]:
        result = []
        if row > 0 and grid[row - 1][col] == "1":
            result.append((row - 1, col))
        if row < len(grid) - 1 and grid[row + 1][col] == "1":
            result.append((row + 1, col))
        if col > 0 and grid[row][col - 1] == "1":
            result.append((row, col - 1))
        if col < len(grid[0]) - 1 and grid[row][col + 1] == "1":
            result.append((row, col + 1))
        return result

    cc = 0
    for row in range(0, len(grid)):
        for col in range(0, len(grid[0])):
            if grid[row][col] == "1":
                explore(row, col)
                cc += 1
    return cc
