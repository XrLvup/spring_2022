# Problem description:  Find K-Length Substrings With No Repeated Characters
"""
Given a string s and an integer k, return the number of substrings in s of length k with no repeated characters.



Example 1:
Input: s = "havefunonleetcode", k = 5
Output: 6
Explanation: There are 6 substrings they are: 'havef','avefu','vefun','efuno','etcod','tcode'.

Example 2:
Input: s = "home", k = 5
Output: 0
Explanation: Notice k can be larger than the length of s. In this case, it is not possible to find any substring.


Constraints:

1 <= s.length <= 104
s consists of lowercase English letters.
1 <= k <= 104
"""


# Time: O(n) - linear scan of the input
# Space: O(k) - storing atmost k keys in the hash map

def numKLenSubstrNoRepeats(s: str, k: int) -> int:
    # edge case for k greater than length of s
    if k > len(s):
        return 0
    # for each sliding windown if the lenght of the dict(keys) = k , then increment counter
    counter = 0
    s_dict = {}
    # first sliding window
    for i in range(0, k):
        if s[i] not in s_dict:
            s_dict[s[i]] = 1
        else:
            s_dict[s[i]] += 1
    if len(s_dict) == k:
        counter += 1
    # rest of the sliding window
    for i in range(k, len(s)):
        if s[i] not in s_dict:
            s_dict[s[i]] = 1
        else:
            s_dict[s[i]] += 1
        # remove the left most edge
        s_dict[s[i - k]] -= 1
        # if count == 0 , delete the key
        if s_dict[s[i - k]] == 0:
            del s_dict[s[i - k]]
        if len(s_dict) == k:
            counter += 1
    return counter
