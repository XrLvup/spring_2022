# Problem description:  Find in Mountain Array
"""
(This problem is an interactive problem.)

You may recall that an array arr is a mountain array if and only if:

arr.length >= 3
There exists some i with 0 < i < arr.length - 1 such that:
arr[0] < arr[1] < ... < arr[i - 1] < arr[i]
arr[i] > arr[i + 1] > ... > arr[arr.length - 1]
Given a mountain array mountainArr, return the minimum index such that mountainArr.get(index) == target. If such an index does not exist, return -1.

You cannot access the mountain array directly. You may only access the array using a MountainArray interface:

MountainArray.get(k) returns the element of the array at index k (0-indexed).
MountainArray.length() returns the length of the array.
Submissions making more than 100 calls to MountainArray.get will be judged Wrong Answer. Also, any solutions that attempt to circumvent the judge will result in disqualification.

Example 1:

Input: array = [1,2,3,4,5,3,1], target = 3
Output: 2
Explanation: 3 exists in the array, at index=2 and index=5. Return the minimum index, which is 2.
Example 2:

Input: array = [0,1,2,4,2,1], target = 3
Output: -1
Explanation: 3 does not exist in the array, so we return -1.


Constraints:

3 <= mountain_arr.length() <= 104
0 <= target <= 109
0 <= mountain_arr.get(index) <= 109
"""
from typing import List

# """
# This is MountainArray's API interface.
# You should not implement it, or speculate about its implementation
# """
#class MountainArray:
#    def get(self, index: int) -> int:
#    def length(self) -> int:

# running time: O(nlog(n))
# space: O(1)

def findInMountainArray(self, target: int, mountain_arr: 'MountainArray') -> int:
    # first find the peak index
    peak_index = self.findPeakIndex(mountain_arr)
    if peak_index == -1:
        return -1  # not a valid mountain array
    if mountain_arr.get(peak_index) == target:
        return peak_index
    asc_index = self.binarySearch(mountain_arr, 0, peak_index - 1, target, True)
    if asc_index != -1:
        return asc_index
    return self.binarySearch(mountain_arr, peak_index + 1, mountain_arr.length() - 1, target, False)


def findPeakIndex(self, mountain_arr: 'MountainArray') -> int:
    length = mountain_arr.length()
    start = 0
    end = length - 1
    while start <= end:
        mid = start + (end - start) // 2
        value = mountain_arr.get(mid)
        if mid > 0 and mid < length - 1 and value > mountain_arr.get(mid - 1) and value > mountain_arr.get(mid + 1):
            return mid
        elif mid < length - 1 and value < mountain_arr.get(mid + 1):
            # part of ascending zone

            start = mid + 1
        else:
            # part of descending zone
            end = mid - 1
    return -1  # should not come here for mountain array


def binarySearch(self, mountain_arr: 'MountainArray', start_index: int, end_index: int, target: int, asc: bool) -> int:
    while start_index <= end_index:
        mid = start_index + (end_index - start_index) // 2
        value = mountain_arr.get(mid)
        if value == target:
            return mid
        elif value < target and asc:
            start_index = mid + 1
        elif value > target and asc:
            end_index = mid - 1
        elif value < target and not asc:
            end_index = mid - 1
        else:
            # value > target and not asc
            start_index = mid + 1
    return -1  # not found in range

