# Problem description:  Subarray Product Less Than K
"""
Given an array of integers nums and an integer k, return the number of contiguous subarrays where the product of all the elements in the subarray is strictly less than k.



Example 1:

Input: nums = [10,5,2,6], k = 100
Output: 8
Explanation: The 8 subarrays that have product less than 100 are:
[10], [5], [2], [6], [10, 5], [5, 2], [2, 6], [5, 2, 6]
Note that [10, 5, 2] is not included as the product of 100 is not strictly less than k.

Example 2:
Input: nums = [1,2,3], k = 0
Output: 0


Constraints:

1 <= nums.length <= 3 * 104
1 <= nums[i] <= 1000
0 <= k <= 106
"""
from typing import List


# running time: O(n)
# space: O(1)

def numSubarrayProductLessThanK(nums: List[int], k: int) -> int:
    # variable sized sliding window problem
    # each i gives you the #of sub-arrays ending at that index
    left = 0
    global_sum = 0
    product = 1
    for i in range(0, len(nums)):
        product = product * nums[i]

        while left <= i and product >= k:
            # move the left pointer
            product = product / nums[left]
            left += 1

        if product < k:
            # print(f"product={product}")
            # print(f"local_sum={i-left+1}, index={i}")
            global_sum += i - left + 1
    return global_sum