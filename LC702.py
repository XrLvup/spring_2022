# Problem description:  Search in a Sorted Array of Unknown Size
"""
This is an interactive problem.

You have a sorted array of unique elements and an unknown size. You do not have an access to the array but you can use the ArrayReader interface to access it. You can call ArrayReader.get(i) that:

returns the value at the ith index (0-indexed) of the secret array (i.e., secret[i]), or
returns 231 - 1 if the i is out of the boundary of the array.
You are also given an integer target.

Return the index k of the hidden array where secret[k] == target or return -1 otherwise.

You must write an algorithm with O(log n) runtime complexity.



Example 1:

Input: secret = [-1,0,3,5,9,12], target = 9
Output: 4
Explanation: 9 exists in secret and its index is 4.

Example 2:
Input: secret = [-1,0,3,5,9,12], target = 2
Output: -1
Explanation: 2 does not exist in secret so return -1.


Constraints:

1 <= secret.length <= 104
-104 <= secret[i], target <= 104
secret is sorted in a strictly increasing order.
"""
from typing import List
# """
# This is ArrayReader's API interface.
# You should not implement it, or speculate about its implementation
# """
class ArrayReader:
    def get(self, index: int) -> int:

# running time: O(log(n))
# space: O(1)

def search(reader, target):
    """
    :type reader: ArrayReader
    :type target: int
    :rtype: int
    """
    # first find the end index
    # done by doubling starting at 1 until it is greater than target
    end = 1
    while reader.get(end) <= target:
        end = end * 2
    # now do regular binay search
    # optimize by having start equal to the previous value of end
    start = end // 2
    while start <= end:
        mid = start + (end - start) // 2
        value = reader.get(mid)
        if value == target:
            return mid
        elif value < target:
            start = mid + 1
        else:
            end = mid - 1
    return -1  # target not found

