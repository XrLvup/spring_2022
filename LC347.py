# Problem description:  Top K Frequent Elements
"""
Example 1:

Input: nums = [1,1,1,2,2,3], k = 2
Output: [1,2]

Example 2:
Input: nums = [1], k = 1
Output: [1]


Constraints:

1 <= nums.length <= 105
k is in the range [1, the number of unique elements in the array].
It is guaranteed that the answer is unique.


Follow up: Your algorithm's time complexity must be better than O(n log n), where n is the array's size.
"""
from typing import List
import random

# Running time: O(n) -  average_case-quick_select and linear scan
# Space: O(n) - building hashmap
def topKFrequent(nums: List[int], k: int) -> List[int]:
    def quick_select(start: int, end: int) -> int:
        if start >= end:
            return start
        rand_index = random.randint(start, end)
        # swap with first element
        tuple_list[start], tuple_list[rand_index] = tuple_list[rand_index], tuple_list[start]
        index = partition(start, end)
        if index == k:
            return index
        elif index < k:
            return quick_select(index + 1, end)
        else:
            return quick_select(start, index - 1)

    def partition(start: int, end: int) -> int:
        pivot = tuple_list[start][1]
        gt = start
        for lt in range(start + 1, end + 1):
            if tuple_list[lt][1] > pivot:
                gt += 1
                tuple_list[gt], tuple_list[lt] = tuple_list[lt], tuple_list[gt]
        # final swap with lt
        tuple_list[gt], tuple_list[start] = tuple_list[start], tuple_list[gt]
        return gt

    # build the frequency dict
    frequency_dict = {}
    for i in range(0, len(nums)):
        if nums[i] not in frequency_dict:
            frequency_dict[nums[i]] = 1
        else:
            frequency_dict[nums[i]] += 1
    # build the tuple list
    tuple_list = []
    for item in frequency_dict:
        tuple_list.append((item, frequency_dict[item]))
    # print(tuple_list)
    # quick-select on tuple list
    n = len(tuple_list)
    index = quick_select(0, n - 1)
    # print(index)
    if index == 0 or k == len(tuple_list):
        return map(lambda x: x[0], tuple_list)
    # print(tuple_list)
    k_freq = tuple_list[:index]
    # print(k_freq)
    result = map(lambda x: x[0], k_freq)
    return result

