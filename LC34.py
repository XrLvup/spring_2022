# Problem description: Find First and Last Position of Element in Sorted Array
"""
Example
1:

Input: nums = [5, 7, 7, 8, 8, 10], target = 8
Output: [3, 4]
Example
2:

Input: nums = [5, 7, 7, 8, 8, 10], target = 6
Output: [-1, -1]
Example
3:

Input: nums = [], target = 0
Output: [-1, -1]

Constraints:

0 <= nums.length <= 105
-109 <= nums[i] <= 109
nums is a
non - decreasing
array.
-109 <= target <= 109
Accepted
929, 235
"""
from typing import List

# running time: O(nlogn)
# space: O(1)

def searchRange(nums: List[int], target: int) -> List[int]:
    if len(nums) == 0:
        return [-1, -1]
    start = 0
    end = len(nums) - 1
    # the starting position of target value
    while start <= end:
        mid = start + (end - start) // 2
        if nums[mid] < target:
            start = mid + 1
        else:  # nums[mid] >= target
            end = mid - 1
    if start >= len(nums) or nums[start] != target:
        return [-1, -1]
    first = start
    end = len(nums) - 1
    # ending position of target value
    while start <= end:
        mid = start + (end - start) // 2
        if nums[mid] <= target:
            start = mid + 1
        else:
            end = mid - 1
    return [first, end]


def main():
    print(searchRange([5, 7, 7, 8, 8, 10], 8))


if __name__ == "__main__":
    main()
