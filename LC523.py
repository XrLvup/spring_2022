# Problem description:  Continuous Subarray Sum
"""
Given an integer array nums and an integer k, return true if nums has a continuous subarray of size at least two whose elements sum up to a multiple of k, or false otherwise.

An integer x is a multiple of k if there exists an integer n such that x = n * k. 0 is always a multiple of k.


Example 1:

Input: nums = [23,2,4,6,7], k = 6
Output: true
Explanation: [2, 4] is a continuous subarray of size 2 whose elements sum up to 6.

Example 2:
Input: nums = [23,2,6,4,7], k = 6
Output: true
Explanation: [23, 2, 6, 4, 7] is an continuous subarray of size 5 whose elements sum up to 42.
42 is a multiple of 6 because 42 = 7 * 6 and 7 is an integer.

Example 3:
Input: nums = [23,2,6,4,7], k = 13
Output: false


Constraints:

1 <= nums.length <= 105
0 <= nums[i] <= 109
0 <= sum(nums[i]) <= 231 - 1
1 <= k <= 231 - 1
"""

# running time: O(n)
# space: O(k)
from typing import List


def checkSubarraySum(nums: List[int], k: int) -> bool:
    prefix_sum = 0
    # key = remainder when div by k , value = length of prefix
    hmap = {0: 0}
    # since subarray should be of size >= 2
    for i in range(0, len(nums)):
        prefix_sum = (prefix_sum + nums[i]) % k
        if prefix_sum in hmap and i + 1 - hmap[prefix_sum] >= 2:
            return True
        if prefix_sum not in hmap:
            hmap[prefix_sum] = i + 1
    return False
