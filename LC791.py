# Problem description:  Custom Sort String
"""
You are given two strings order and s. All the words of order are unique and were sorted in some custom order previously.

Permute the characters of s so that they match the order that order was sorted. More specifically, if a character x occurs before a character y in order, then x should occur before y in the permuted string.

Return any permutation of s that satisfies this property.



Example 1:
Input: order = "cba", s = "abcd"
Output: "cbad"
Explanation:
"a", "b", "c" appear in order, so the order of "a", "b", "c" should be "c", "b", and "a".
Since "d" does not appear in order, it can be at any position in the returned string. "dcba", "cdba", "cbda" are also valid outputs.

Example 2:
Input: order = "cbafg", s = "abcd"
Output: "cbad"


Constraints:

1 <= order.length <= 26
1 <= s.length <= 200
order and s consist of lowercase English letters.
All the characters of order are unique.
"""
from typing import List


# running time: O(n)
# space: O(n)
def customSortString(order: str, s: str) -> str:
    # keep track of count of char in s
    counter_s = {}
    for c in s:
        if c not in counter_s:
            counter_s[c] = 1
        else:
            counter_s[c] += 1
    output_list = []
    for s in order:
        if s in counter_s:
            output_list.append((s * counter_s[s]))
            counter_s[s] = 0
    # iterate through counter_s to get remaining elements not specified in order
    for k in counter_s:
        output_list.append((k * counter_s[k]))
    return "".join(output_list)
