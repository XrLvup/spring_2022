# Problem description:   Missing Element in Sorted Array
"""
Given an integer array nums which is sorted in ascending order and all of its elements are unique and given also an integer k, return the kth missing number starting from the leftmost number of the array.



Example 1:

Input: nums = [4,7,9,10], k = 1
Output: 5
Explanation: The first missing number is 5.


Example 2:
Input: nums = [4,7,9,10], k = 3
Output: 8
Explanation: The missing numbers are [5,6,8,...], hence the third missing number is 8.

Example 3:
Input: nums = [1,2,4], k = 3
Output: 6
Explanation: The missing numbers are [3,5,6,7,...], hence the third missing number is 6.


Constraints:

1 <= nums.length <= 5 * 104
1 <= nums[i] <= 107
nums is sorted in ascending order, and all the elements are unique.
1 <= k <= 108
"""
from typing import List


# running time: O(log(n))
# space: O(1)

def missingElement(nums: List[int], k: int) -> int:
    start = 0
    end = len(nums) - 1
    while start <= end:
        mid = start + (end - start) // 2
        # print(mid)
        expected_no = nums[0] + mid
        actual_no = nums[mid]
        missing_nos = actual_no - expected_no
        # print(f"actual={actual_no}, expected={expected_no}, missing_nos={missing_nos}")
        if missing_nos < k:
            start = mid + 1
            # print("xxx")
        else:  # >= k
            end = mid - 1
            # print("yyy")
    expected_no = nums[0] + end
    actual_no = nums[end]
    missing_nos = actual_no - expected_no
    k = k - missing_nos
    return nums[end] + k
