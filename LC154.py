# Problem description:  Find Minimum in Rotated Sorted Array II
"""
Suppose an array of length n sorted in ascending order is rotated between 1 and n times. For example, the array nums = [0,1,4,4,5,6,7] might become:

[4,5,6,7,0,1,4] if it was rotated 4 times.
[0,1,4,4,5,6,7] if it was rotated 7 times.
Notice that rotating an array [a[0], a[1], a[2], ..., a[n-1]] 1 time results in the array [a[n-1], a[0], a[1], a[2], ..., a[n-2]].

Given the sorted rotated array nums that may contain duplicates, return the minimum element of this array.

You must decrease the overall operation steps as much as possible.



Example 1:

Input: nums = [1,3,5]
Output: 1
Example 2:

Input: nums = [2,2,2,0,1]
Output: 0


Constraints:

n == nums.length
1 <= n <= 5000
-5000 <= nums[i] <= 5000
nums is sorted and rotated between 1 and n times.


Follow up: This problem is similar to Find Minimum in Rotated Sorted Array, but nums may contain duplicates. Would this affect the runtime complexity? How and why?
"""
from typing import List


# running time: O(n) - since need the forward walk in the beginning to remove duplicates when comparing with last element
# space: O(1)

def findMin(nums: List[int]) -> int:
    # now remove duplicates from start
    # so essentially walk forward until first element is different from last
    start = 0
    while start < len(nums) and nums[start] == nums[-1]:
        start += 1
    # check required if all elements are the same
    if start >= len(nums):
        return nums[start - 1]
    # now have the pattern of the two ascending zones and same as min in rotated sorted array-1
    end = len(nums) - 1
    while start <= end:
        mid = start + (end - start) // 2
        # compare with last element to find the correct zone since min is in zone-2
        if nums[mid] > nums[-1]:  # in zone1 so move to zone2
            start = mid + 1
        else:  # in zone2 but move to leftmost element to find min
            end = mid - 1
    return nums[start]

